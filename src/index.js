// @flow

import './style.css'

const header: HTMLHeadingElement = document.createElement('h1')
header.innerText = '🚀 andrew-pyle/dotenv Configuration is Working! 🚀'

if (document.body !== null) {
  document.body.appendChild(header)
} else {
  throw new Error('document.body was null')
}
