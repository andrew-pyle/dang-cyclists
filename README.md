# Dang Cyclists
A website to put cars and cyclists at ease with each other

## For Cars
- 3 feet of clearance when passing bicycles
- Bicycles may use ANY public road, except freeways
- They may also use the WHOLE lane

## For Bicycles
- Always ride WITH traffic
- Use a light at night

## Know the Law
